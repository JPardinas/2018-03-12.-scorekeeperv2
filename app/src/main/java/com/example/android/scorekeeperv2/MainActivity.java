package com.example.android.scorekeeperv2;

import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;
import java.util.Random;

import static android.view.View.VISIBLE;

public class MainActivity extends AppCompatActivity {

    //Constants for time
    private static final long START_TIME_IN_MILLIS = 60000;
    protected Button mButtonTripleATeam;
    protected Button mButtonTripleBTeam;
    protected Button mButtonDoubleATeam;
    protected Button mButtonDoubleBTeam;
    protected Button mButtonFreeThrowATeam;
    protected Button mButtonFreeThrowBTeam;

    //Left shoots for free throws and counter for the shoots for each team
    int mTripleATeam = 0;
    int mTripleBTeam = 0;
    int mDoubleATeam = 0;
    int mDoubleBTeam = 0;
    int mFailATeam = 0;
    int mFailBTeam = 0;
    int mFoulATeam = 0;
    int mFoulBTeam = 0;
    int mFreeThrowATeam = 0;
    int mFreeTrhowBTeam = 0;
    int mFreeThrow = 2;

    //Time left
    private long mTimeLeftInMillis = START_TIME_IN_MILLIS;

    //Countdowntimer
    private CountDownTimer mCountDownTimer;

    //Buttons
    private Button mButtonStartPause;
    private Button mButtonReset;

    //Random numbers
    private Random mRandom = new Random(System.nanoTime());

    //TextViews
    private TextView mTextViewATeamScore;
    private TextView mTextViewBTeamScore;
    private TextView mTextViewCountDown;
    private TextView mTextViewLogATeam;
    private TextView mTextViewLogBTeam;

    //Teams scores
    private int mATeamScore;
    private int mBTeamScore;

    //Booleans to know if timmer is running and a new game starts
    private boolean mTimerRunning;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Assigng the TextViews
        mTextViewATeamScore = findViewById(R.id.text_a_score);
        mTextViewBTeamScore = findViewById(R.id.text_b_score);
        mTextViewCountDown = findViewById(R.id.text_view_countdown);
        mTextViewLogATeam = findViewById(R.id.text_log_a);
        mTextViewLogBTeam = findViewById(R.id.text_log_b);

        //Assign the buttons
        mButtonStartPause = findViewById(R.id.button_start_pause);
        mButtonReset = findViewById(R.id.button_reset);
        mButtonTripleATeam = findViewById(R.id.button_triple_a_team);
        mButtonTripleBTeam = findViewById(R.id.button_triple_b_team);
        mButtonDoubleATeam = findViewById(R.id.button_double_a_team);
        mButtonDoubleBTeam = findViewById(R.id.button_double_b_team);
        mButtonFreeThrowATeam = findViewById(R.id.button_free_throw_a_team);
        mButtonFreeThrowBTeam = findViewById(R.id.button_free_throw_b_team);

        //Initialice autostroll in the log
        mTextViewLogATeam.setMovementMethod(new ScrollingMovementMethod());
        mTextViewLogBTeam.setMovementMethod(new ScrollingMovementMethod());


        //Assign onclicklistener to the A team Triple button
        mButtonTripleATeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Conditionals to score or not
                if (mTimerRunning) {
                    ATeamTriple();
                }

            }
        });

        //Assign onclicklistener to the B team Triple button
        mButtonTripleBTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Conditionals to score or not
                if (mTimerRunning) {
                    BTeamTriple();
                }

            }
        });

        //Assign onclicklistener to the A team Double button
        mButtonDoubleATeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Conditionals to score or not
                if (mTimerRunning) {
                    ATeamDouble();
                }

            }
        });

        //Assign onclicklistener to the B team Double button
        mButtonDoubleBTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Conditionals to score or not
                if (mTimerRunning) {
                    BTeamDouble();
                }

            }
        });

        //Assign onclicklistener to the A team Free Throw button
        mButtonFreeThrowATeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ATeamFreeThrow();

            }
        });

        //Assign onclicklistener to the B team Free Throw button
        mButtonFreeThrowBTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                BTeamFreeThrow();

            }
        });

        //Assign onclicklistener to the start-pause button
        mButtonStartPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Conditionals to call tge start / pause time methods
                if (mTimerRunning) {
                    pauseTimer();
                } else {
                    startTimer();
                }

            }
        });

        //Assign onclicklistener to the reset button
        mButtonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resetGame();

            }
        });

        //Call method to update the countdown
        updateCountDownText();

    }

    //Start Timer method
    private void startTimer() {

        mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                mTimeLeftInMillis = millisUntilFinished;
                updateCountDownText();

            }

            //Methods and values when the time finish
            @Override
            public void onFinish() {
                mTimerRunning = false;
                mButtonStartPause.setText(R.string.start_button);
                mButtonStartPause.setVisibility(View.INVISIBLE);
                mButtonReset.setVisibility(VISIBLE);
                mTextViewCountDown.setText("00:00");

                //Method to compare and decide the winner
                winner();

            }
        }.start();

        mTimerRunning = true;
        mButtonStartPause.setText(R.string.pause_button);
        mButtonReset.setVisibility(View.INVISIBLE);
    }

    //Method to pause the countdown time and the game
    private void pauseTimer() {
        mCountDownTimer.cancel();
        mTimerRunning = false;
        mButtonStartPause.setText(R.string.start_button);
        mButtonReset.setVisibility(VISIBLE);
    }

    //Method to reset the countdown time, the scores and log
    private void resetGame() {

        mTimeLeftInMillis = START_TIME_IN_MILLIS;
        updateCountDownText();
        mButtonReset.setVisibility(View.INVISIBLE);
        mButtonStartPause.setVisibility(VISIBLE);

        //Game info
        mTextViewLogATeam.setText(String.format(getString(R.string.game_info_a_team)));
        mTextViewLogBTeam.setText(String.format(getString(R.string.game_info_b_team)));

        //Team Scores
        mBTeamScore = 0;
        mTextViewBTeamScore.setText("" + mBTeamScore);
        mATeamScore = 0;
        mTextViewATeamScore.setText("" + mBTeamScore);

        //Free throw buttons invisible
        mButtonFreeThrowATeam.setVisibility(View.INVISIBLE);
        mButtonFreeThrowBTeam.setVisibility(View.INVISIBLE);

        //Game Final Info
        mTripleATeam = 0;
        mTripleBTeam = 0;
        mDoubleATeam = 0;
        mDoubleBTeam = 0;
        mFailATeam = 0;
        mFailBTeam = 0;
        mFoulATeam = 0;
        mFoulBTeam = 0;
        mFreeThrowATeam = 0;
        mFreeTrhowBTeam = 0;

    }

    //Method to update the countdown time and format to minutes and seconds
    private void updateCountDownText() {

        int minutes = (int) (mTimeLeftInMillis / 1000) / 60;
        int seconds = (int) (mTimeLeftInMillis / 1000) % 60;

        String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);

        mTextViewCountDown.setText(timeLeftFormatted);
    }

    //Method to show a toast with the winner and the stadistics of each team
    public void winner() {

        Toast mToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
        TextView mWinner = (TextView) mToast.getView().findViewById(android.R.id.message);
        mWinner.setGravity(Gravity.FILL_HORIZONTAL | Gravity.CENTER);

        if (mATeamScore >= mBTeamScore) {
            if (mATeamScore == mBTeamScore) {
                mToast.setText("DRAW!");
            } else {
                mToast.setText("Team A WON!");
            }
        } else {
            mToast.setText("Team BWON!");
        }

        mToast.show();
        stadistics();
    }

    //Method to score triple the A team
    private void ATeamTriple() {

        //Get random number to decide if it is a triple, fail or a foul - Fail and Foul has a chance of 1/6 each of them
        int aux = mRandom.nextInt(6);

        //Triple
        if (aux <= 3) {
            mTripleATeam++;
            mATeamScore = mATeamScore + 3;
            mTextViewATeamScore.setText("" + mATeamScore);

            if (mTextViewLogATeam.getText().equals("")) {
                mTextViewLogATeam.append("Your team scored 3 points!");
            } else {
                mTextViewLogATeam.append("\r\nYour team scored 3 points!");
            }

            //Fail or foul
        } else {

            //Fail
            if (aux == 4) {
                mFailATeam++;
                if (mTextViewLogATeam.getText().equals("")) {
                    mTextViewLogATeam.append("Your team failed!");
                } else {
                    mTextViewLogATeam.append("\r\nYour team failed!");
                }

                //Foul
            } else {
                pauseTimer();
                mFoulATeam++;
                mButtonReset.setVisibility(View.INVISIBLE);
                mButtonFreeThrowBTeam.setVisibility(VISIBLE);
                mButtonStartPause.setVisibility(View.INVISIBLE);
                if (mTextViewLogATeam.getText().equals("")) {
                    mTextViewLogATeam.append("Your team did a foul! 2 Free throws for the another team");
                } else {
                    mTextViewLogATeam.append("\r\nYour team did a foul! 2 Free throws for the another team");
                }
            }
        }

    }

    //Method to score triple the B team
    private void BTeamTriple() {

        //Get random number to decide if it is a triple, fail or a foul - Fail and Foul has a chance of 1/6 each of them
        int aux = mRandom.nextInt(6);

        //Triple
        if (aux <= 3) {
            mTripleBTeam++;
            mBTeamScore = mBTeamScore + 3;
            mTextViewBTeamScore.setText("" + mBTeamScore);

            if (mTextViewLogBTeam.getText().equals("")) {
                mTextViewLogBTeam.append("Your team scored 3 points!");
            } else {
                mTextViewLogBTeam.append("\r\nYour team scored 3 points!");
            }

            //Fail or foul
        } else {

            //Fail
            if (aux == 4) {
                mFailBTeam++;
                if (mTextViewLogBTeam.getText().equals("")) {
                    mTextViewLogBTeam.append("Your team failed!");
                } else {
                    mTextViewLogBTeam.append("\r\nYour team failed!");
                }

                //Foul
            } else {
                pauseTimer();
                mFoulBTeam++;
                mButtonReset.setVisibility(View.INVISIBLE);
                mButtonFreeThrowATeam.setVisibility(VISIBLE);
                mButtonStartPause.setVisibility(View.INVISIBLE);
                if (mTextViewLogBTeam.getText().equals("")) {
                    mTextViewLogBTeam.append("Your team did a foul! 2 Free throws for the another team");
                } else {
                    mTextViewLogBTeam.append("\r\nYour team did a foul! 2 Free throws for the another team");
                }
            }
        }

    }

    //Method to score double the A team
    private void ATeamDouble() {

        //Get random number to decide if it is a double, fail or a foul - Fail and Foul has a chance of 1/10 each of them
        int aux = mRandom.nextInt(10);

        //Double
        if (aux <= 7) {
            mDoubleATeam++;
            mATeamScore = mATeamScore + 2;
            mTextViewATeamScore.setText("" + mATeamScore);

            if (mTextViewLogATeam.getText().equals("")) {
                mTextViewLogATeam.append("Your team scored 2 points!");
            } else {
                mTextViewLogATeam.append("\r\nYour team scored 2 points!");
            }

            //Fail or foul
        } else {

            //Fail
            if (aux == 8) {
                mFailATeam++;
                if (mTextViewLogATeam.getText().equals("")) {
                    mTextViewLogATeam.append("Your team failed!");
                } else {
                    mTextViewLogATeam.append("\r\nYour team failed!");
                }

                //Foul
            } else {
                pauseTimer();
                mFoulATeam++;
                mButtonReset.setVisibility(View.INVISIBLE);
                mButtonFreeThrowBTeam.setVisibility(VISIBLE);
                mButtonStartPause.setVisibility(View.INVISIBLE);
                if (mTextViewLogATeam.getText().equals("")) {
                    mTextViewLogATeam.append("Your team did a foul! 2 Free throws for the another team");
                } else {
                    mTextViewLogATeam.append("\r\nYour team did a foul! 2 Free throws for the another team");
                }
            }
        }

    }

    //Method to score double the B team
    private void BTeamDouble() {

        //Get random number to decide if it is a double, fail or a foul - Fail and Foul has a chance of 1/10 each of them
        int aux = mRandom.nextInt(10);

        //Double
        if (aux <= 7) {
            mDoubleBTeam++;
            mBTeamScore = mBTeamScore + 2;
            mTextViewBTeamScore.setText("" + mBTeamScore);

            if (mTextViewLogBTeam.getText().equals("")) {
                mTextViewLogBTeam.append("Team B scored 2 points!");
            } else {
                mTextViewLogBTeam.append("\r\nTeam B scored 2 points!");
            }

            //Fail or foul
        } else {

            //Fail
            if (aux == 8) {
                mFailBTeam++;
                if (mTextViewLogBTeam.getText().equals("")) {
                    mTextViewLogBTeam.append("Your team failed!");
                } else {
                    mTextViewLogBTeam.append("\r\nYour team failed!");
                }

                //Foul
            } else {
                pauseTimer();
                mFoulBTeam++;
                mButtonReset.setVisibility(View.INVISIBLE);
                mButtonFreeThrowATeam.setVisibility(VISIBLE);
                mButtonStartPause.setVisibility(View.INVISIBLE);
                if (mTextViewLogBTeam.getText().equals("")) {
                    mTextViewLogBTeam.append("Your team did a foul! 2 Free throws for the another team");
                } else {
                    mTextViewLogBTeam.append("\r\nYour team did a foul! 2 Free throws for the another team");
                }
            }
        }

    }

    private void ATeamFreeThrow() {

        //Rest 1 shoot
        mFreeThrow--;

        //Get random number to decide if it is a fail - Fail has a chance of 1/10
        int aux = mRandom.nextInt(10);

        //Conditional - 1/10 chances of fail
        if (aux <= 8) {
            mFreeThrowATeam++;
            mATeamScore = mATeamScore + 1;
            mTextViewATeamScore.setText("" + mATeamScore);

            if (mTextViewLogATeam.getText().equals("")) {
                mTextViewLogATeam.append("Your team scored 1 points!");
            } else {
                mTextViewLogATeam.append("\r\nYour team scored 1 points!");
            }
            //Fail
        } else {
            mFailATeam++;
            if (mTextViewLogATeam.getText().equals("")) {
                mTextViewLogATeam.append("Your team failed!");
            } else {
                mTextViewLogATeam.append("\r\nYour team failed!");
            }

        }

        //Continue game when the team did the 2 shoots
        if (mFreeThrow == 0) {
            mButtonStartPause.setVisibility(View.VISIBLE);
            mButtonFreeThrowATeam.setVisibility(View.INVISIBLE);
            mFreeThrow = 2;
            startTimer();
        }

    }

    private void BTeamFreeThrow() {

        //Rest 1 shoot
        mFreeThrow--;

        //Get random number to decide if it is fail - Fail has a chance of 1/10
        int aux = mRandom.nextInt(10);

        //Conditional - 1/10 chances of fail
        if (aux <= 8) {
            mFreeTrhowBTeam++;
            mBTeamScore = mBTeamScore + 1;
            mTextViewBTeamScore.setText("" + mBTeamScore);

            if (mTextViewLogBTeam.getText().equals("")) {
                mTextViewLogBTeam.append("Your team scored 1 points!");
            } else {
                mTextViewLogBTeam.append("\r\nYour team scored 1 points!");
            }

            //Fail
        } else {
            mFailBTeam++;
            if (mTextViewLogBTeam.getText().equals("")) {
                mTextViewLogBTeam.append("Your team failed!");
            } else {
                mTextViewLogBTeam.append("\r\nYour team failed!");
            }

        }

        //Continue game when the team did the 2 shoots
        if (mFreeThrow == 0) {
            mButtonStartPause.setVisibility(View.VISIBLE);
            mButtonFreeThrowBTeam.setVisibility(View.INVISIBLE);
            mFreeThrow = 2;
            startTimer();
        }

    }

    //Method to show the stadistics of each team
    private void stadistics() {

        //Blank the textviews of each team
        mTextViewLogATeam.setText(String.format(getString(R.string.game_info_a_team)));
        mTextViewLogBTeam.setText(String.format(getString(R.string.game_info_b_team)));

        //Show stadistics for Team A
        mTextViewLogATeam.append("Stadistics for your Team:");
        mTextViewLogATeam.append("\r\n");
        mTextViewLogATeam.append("Triples: " + mTripleATeam);
        mTextViewLogATeam.append("\r\nDoubles: " + mDoubleATeam);
        mTextViewLogATeam.append("\r\nFree Throws: " + mFreeThrowATeam);
        mTextViewLogATeam.append("\r\nFails: " + mFailATeam);
        mTextViewLogATeam.append("\r\nFouls: " + mFoulATeam);

        //Show stadistics for Team B
        mTextViewLogBTeam.append("Stadistics for your Team:");
        mTextViewLogBTeam.append("\r\n");
        mTextViewLogBTeam.append("Triples: " + mTripleBTeam);
        mTextViewLogBTeam.append("\r\nDoubles: " + mDoubleBTeam);
        mTextViewLogBTeam.append("\r\nFree Throws: " + mFreeTrhowBTeam);
        mTextViewLogBTeam.append("\r\nFails: " + mFailBTeam);
        mTextViewLogBTeam.append("\r\nFouls: " + mFoulBTeam);

        //Show if your team wins or not
        if (mATeamScore >= mBTeamScore) {
            if (mATeamScore == mBTeamScore) {
                mTextViewLogATeam.append("\r\n");
                mTextViewLogATeam.append("DRAW!!");
                mTextViewLogBTeam.append("\r\n");
                mTextViewLogBTeam.append("DRAW!!");
            } else {
                mTextViewLogATeam.append("\r\n");
                mTextViewLogATeam.append("You WIN!!");
                mTextViewLogBTeam.append("\r\n");
                mTextViewLogBTeam.append("You LOSE!!");
            }
        } else {
            mTextViewLogATeam.append("\r\n");
            mTextViewLogATeam.append("You LOSE!!");
            mTextViewLogBTeam.append("\r\n");
            mTextViewLogBTeam.append("You WIN!!");
        }

    }

}
